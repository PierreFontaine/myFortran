program test
  use test_hello

  IMPLICIT NONE
  type(Student) :: s

  s%nom = "fontaine"
  s%prenom = "pierre"
  s%filiere = "informatique"

  call hello()

  call printName(s)
end
