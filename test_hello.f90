
module test_hello
TYPE::Student
  CHARACTER(len=30) nom
  CHARACTER(len=30) prenom
  CHARACTER(len=30) filiere
END TYPE Student
contains

  subroutine hello
    print *, "hello world"
  end subroutine hello

  subroutine printName(s)
    type(Student), intent(in) :: s
    print *, s%nom
  end subroutine printName
end module test_hello
