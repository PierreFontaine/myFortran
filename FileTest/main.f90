PROGRAM test
  implicit none

  integer :: a
  integer :: control
  logical ::existe

  inquire( file="exemple", exist=existe)

  if (existe) then
    open(unit=10,file="exemple",status="OLD",action="READ",position="rewind")
    read(unit=10,iostat=control),a
    print *,a
    read(unit=10,iostat=control),a
    print *,a
    close(unit=10)
  end if

END
