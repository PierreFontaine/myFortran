module testMod

  implicit none
  type t_a
    integer :: a
    integer :: b
  end type

  type t_b
    integer :: a
    integer :: b
  end type
contains
  subroutine initTA(t,a,b)
    type(t_a), intent(inout) :: t
    integer, intent(in) :: a
    integer, intent(inout) :: b

    t%a = a
    t%b = b
  end subroutine

end module testMod
