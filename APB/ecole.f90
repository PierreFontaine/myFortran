module ecole

  use formation

  implicit none

  type t_ecole
    character(len=30)             :: nom
    integer                       :: nb_formation
    type(t_formation),dimension(10) :: tab_formation
  end type t_ecole

contains

  ! fonction retournant le nom d'une instance de type
  ! ecole
  function ecoleGetNom(e)
    character(len=30) :: ecoleGetNom
    type(t_ecole)     :: e

    ecoleGetNom = e%nom
  end function

end module ecole
