module InAndOut
  use etudiant
  use affectation
  use note
  use concours
  implicit none

  ! RAPPEL DE LA STRUCTURE ETUDIANT

  ! type t_etudiant
  !   character(len=30)               :: nom
  !   character(len=30)               :: prenom
  !   integer                         :: nb_concours
  !   type(t_note), dimension(5)      :: tab_note
  !   integer                         :: nb_voeux
  !   type(t_formation),dimension(5)  :: tab_voeux
  !   integer                         :: affectation
  ! end type t_etudiant
contains
  ! une procedure qui depuis un fichier rempli un tableau d'étudiant
  subroutine inAndOutRemplirTabEtud(t,n,f,c,form,nForm)
    type(t_etudiant),dimension(100),intent(out) :: t
    type(t_concours),dimension(:),intent(in) :: c
    integer,intent(out) :: n
    character(*),intent(in) :: f
    type(t_formation),dimension(:),intent(in) :: form
    integer, intent(in) :: nForm

    integer :: control
    type(t_etudiant) :: e
    integer :: i
    integer :: j
    logical :: existe
    type(t_concours) :: tmp_c
    character(30) :: tmp_s

    i = 0
    inquire(file=f,exist=existe)
    if (existe) then
      open(unit=10,file=f,status="OLD",action="READ",position="REWIND")
      control = 0
      do while ( control == 0 )
        i = i + 1
        print *,"i vaut : ",i
        read(10,*,iostat=control)t(i)%nom,t(i)%prenom,t(i)%nb_concours
        print *,"nom et prenom : ",t(i)%prenom,t(i)%nom

        do j = 1, t(i)%nb_concours, 1
          read(10,*,iostat=control)tmp_s
          t(i)%tab_note(j)%c= getConcoursParNom(c,size(c),tmp_s)
        end do

        do j = 1, t(i)%nb_concours, 1
          read(10,*,iostat=control)t(i)%tab_note(j)%note
        end do

        read(10,*,iostat=control)t(i)%nb_voeux


        do j = 1, t(i)%nb_voeux, 1
          read(10,*,iostat=control)tmp_s
          print *,tmp_s
          t(i)%tab_voeux(j) = getFormationParNom(form,nForm,tmp_s)
        end do

        t(i)%affectation = 1
        print *,control
        print *,i
      end do
      n = i - 1 ! WORKAROUND DEGUEULASSE
      print *, "n vaut ",  n
      ! call etudiantTableauAffichage(t,n)
      close(unit = 11, status = "KEEP")
    else
      print *,"erreur, le fichier demandé n'existe pas" ! gestion d'exception
    end if
  end subroutine

  ! une fonction qui sort les affectations
  ! format JSON peut etre ?
  subroutine inAndOutPrintOut(t,n,f)
    type(t_affectation),dimension(:),intent(in) :: t
    integer,intent(in) :: n
    character(*),intent(in) :: f
    integer :: i

    open(unit = 11, file = f, status = "UNKNOWN", action = "WRITE", position = "REWIND")

    do i = 1, n, 1
      write(11,*) " nom : ",t(i)%etudiant%nom
      write(11,*) " nom : ",t(i)%etudiant%prenom
      write(11,*) " formation : ",t(i)%formation%nom
      write(11,*) " -------- "
    end do

    close(unit = 11, status = "KEEP")


  end subroutine


end module InAndOut
