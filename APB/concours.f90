module concours

  use formation

  implicit none

  type t_concours
    character(len=30)                 :: nom
    type(t_formation), dimension(10)  :: tab_formation
    integer                           :: nb_formation
  end type t_concours

contains

  function creerConcours(nom,f)
    character(*), intent(in) :: nom
    type(t_formation), dimension(:), intent(in) :: f
    type(t_concours) :: creerConcours
    type(t_concours) :: c
    integer :: i

    c%nom = nom
    c%nb_formation = 0

    do i = 1,size(f),1
      c%tab_formation(i) = f(i)
    end do
    c%nb_formation = size(f)
    creerConcours = c
  end function

  ! fonction qui retourne si etant donnée une formation
  ! le concours en donne l'accès
  function formationAccesConcours(f,c)
    type(t_formation),intent(in) :: f
    type(t_concours), intent(in) :: c
    logical                      :: formationAccesConcours
    logical :: trouve
    integer :: i

    trouve = .FALSE.
    i = 1
    do while ( (trouve .eqv. .FALSE.) .AND. (i <= c%nb_formation))
      if(c%tab_formation(i)%nom == f%nom) then
        trouve = .TRUE.
      end if
      i = i+1
    end do
    formationAccesConcours = trouve
  end function

  ! étant donné un une formation, retourne le concours
  ! associé
  function getConcoursAssocie(f,c)
    type(t_formation), intent(in) :: f
    type(t_concours), dimension(:),intent(in) ::c
    type(t_concours) :: getConcoursAssocie
    type(t_concours) :: res
    integer :: i

    if(size(c) > 0)then
      do i = 1, size(c), 1
        if(formationAccesConcours(f,c(i))) then
          res = c(i)
        end if
      end do
    end if
    getConcoursAssocie = res
  end function

  ! fonction qui etant donné un nom et un tableau de concours retourne
  ! le concours associé
  ! note : utile pour la lecture d'étudiant par fichier
  function getConcoursParNom(t,n,c)
    type(t_concours),dimension(:),intent(in) :: t
    integer, intent(in) :: n
    character(*), intent(in) :: c
    type(t_concours) :: getConcoursParNom
    logical :: trouve
    integer :: i
    type(t_concours) :: res

    i = 1
    trouve = .FALSE.

    do while ( (trouve .eqv. .FALSE.) .AND. (i <= n))
      if(t(i)%nom == c) then
        res = t(i)
        trouve = .TRUE.
      end if
      i = i + 1
    end do

    getConcoursParNom = res
  end function
end module concours
