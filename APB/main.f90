PROGRAM APB

  ! DECLARATION DES USES
  use etudiant
  use formation
  use note
  use concours
  use affectation
  use InAndOut

  ! DECLARATION DES VARIABLES
  IMPLICIT NONE

  integer :: i
  integer :: j
  integer :: k
  type(t_concours) :: tmp_conc
  type(t_formation)   :: tmp_form
  type(t_affectation) :: tmp_aff
  type(t_affectation),dimension(100) :: resAPB

  ! Workaround pour contrer le fait que y'a pas de liste ...
  type(t_etudiant),dimension(100) :: tabEleveSurplus
  integer :: n_eleve_surplus

  ! c'est sur ce tableau qu'on se refere pour gérer le nombre de place restante
  ! dans chaque formation
  type(t_formation),dimension(100) :: tabFormation
  integer :: nbFormation
  integer :: nEleveAffecte

  ! ****************************************************************************
  !                     DECLARATION DES FORMATIONS
  ! ****************************************************************************
  type(t_formation) :: ENSGTI   = t_formation('ENSGTI',1,0)
  type(t_formation) :: ENSICAEN = t_formation('ENSICAEN',1,0)
  type(t_formation) :: ENSEIRB  = t_formation('ENSEIRB',2,0)
  type(t_formation) :: ENSEC  = t_formation('ENSEC',1,0)
  type(t_formation) :: ENSIMAG  = t_formation('ENSIMAG',3,0)
  type(t_formation) :: ENSIBS  = t_formation('ENSIBS',1,0)
  type(t_formation) :: RECAL  = t_formation('RECAL',100,0)


  ! ****************************************************************************
  !                     DECLARATION DES CONCOURS
  ! ****************************************************************************
  type(t_concours) :: CCP
  type(t_concours) :: MINE
  type(t_concours) :: POLY

  ! ****************************************************************************
  !                     DECLARATION DE LA COLLECTION ETUDIANT
  ! ****************************************************************************
  type(t_etudiant), dimension(100) :: collection_etudiant
  integer :: nb_collection_etud

  ! ****************************************************************************
  !                     INITIALISATION DE VARS
  ! ****************************************************************************

  nbFormation = 0
  nb_collection_etud = 0
  nEleveAffecte = 0

  call ajouterFormations(tabFormation,nbFormation,(/ENSGTI,ENSICAEN,ENSEIRB,ENSEC,ENSIMAG,ENSIBS,RECAL/))

  CCP = creerConcours('CCP', (/ENSGTI,ENSICAEN,RECAL/))
  MINE = creerConcours('MINES', (/ENSEIRB,ENSEC,RECAL/))
  POLY = creerConcours('POLYTECHNIQUE',(/ENSIMAG,ENSIBS,RECAL/))

   print *,"appel de la routine pour lire le fichier"
   call inAndOutRemplirTabEtud(collection_etudiant,nb_collection_etud,"data.dat",(/CCP,MINE,POLY/),tabFormation,nbFormation)



 ! ****************************************************************************
 !                     COEUR DU PROGRAMME
 ! ****************************************************************************

  do i = 1, nb_collection_etud, 1
    tmp_aff = (t_affectation(collection_etudiant(i),etudiantNVoeux(collection_etudiant(i),collection_etudiant(i)%affectation)))
    call affectationAjoutCollection(tmp_aff,resAPB,nEleveAffecte)
    call formationIncrementPlaceEleve(tabFormation,etudiantNVoeux(collection_etudiant(i),1),nbFormation)
  end do

  print *, ""
  print *, "lecture des eleves affectés n = ",nEleveAffecte
  print *, ""
  call affectationToString(resAPB,nEleveAffecte)

  print *, ""
  print *, "routine de resolution"
  print *, ""

  do while ( formationSurplus(tabFormation,nbFormation) )
    tmp_form = formationGetSurplus(tabFormation,nbFormation)
    print *, tmp_form%nom, "", tmp_form%nb_place, "", tmp_form%nb_eleve
    call affectationGetEleveEnSurplus(resAPB,nEleveAffecte,tmp_form,tabEleveSurplus,n_eleve_surplus)

    call etudiantTableauAffichage(tabEleveSurplus,n_eleve_surplus)

    tmp_conc = getConcoursAssocie(tmp_form,(/CCP,MINE,POLY/))

    call etudiantTri(tabEleveSurplus,tmp_conc,n_eleve_surplus)

    print *, ""
    print *, "tri des étudiants"
    print *, ""

    call etudiantTableauAffichage(tabEleveSurplus,n_eleve_surplus)

    print *, ""
    print *, "la formation peut garder que ", tmp_form%nb_place," étudiants"
    print *, "re affectation de ",n_eleve_surplus - tmp_form%nb_place ," personnes"
    print *, ""
    call formationArrayToString(tabFormation,nbFormation)
    do i = n_eleve_surplus - tmp_form%nb_place + 1, n_eleve_surplus , 1
      print *,i," : ",tabEleveSurplus(i)%nom
      call formationDecrementPlaceEleve(tabFormation,tmp_form,nbFormation)
      call affectationIncrementAffectationEleve(resAPB,tabEleveSurplus(i),nEleveAffecte)
      k = tabEleveSurplus(i)%affectation + 1
      call formationIncrementPlaceEleve(tabFormation,etudiantNVoeux(tabEleveSurplus(i),k),nbFormation)
    end do
    call formationArrayToString(tabFormation,nbFormation)

    do i = 1, nEleveAffecte, 1
      resAPB(i)%formation = etudiantNVoeux(resAPB(i)%etudiant,resAPB(i)%etudiant%affectation)
    end do

    print *, ""
    print *, "lecture des eleves affectés n = ",nEleveAffecte
    print *, ""

  end do
  call affectationToString(resAPB,nEleveAffecte)
  call inAndOutPrintOut(resAPB,nEleveAffecte,"out.json")
! 61 eme anniversaire de fortran
END
