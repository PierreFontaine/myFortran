module Etudiant
  use note
  use formation

  implicit none

  type t_etudiant
    character(len=30)               :: nom
    character(len=30)               :: prenom
    integer                         :: nb_concours
    type(t_note), dimension(5)      :: tab_note
    integer                         :: nb_voeux
    type(t_formation),dimension(20)  :: tab_voeux
    integer                         :: affectation
  end type t_etudiant

contains
  ! fonction pour construire un etudiant
  function creerEtudiant(nom,prenom)
    character(*),intent(in) :: nom
    character(*),intent(in) :: prenom
    type(t_etudiant)        :: creerEtudiant

    type(t_etudiant) :: e

    e%nom = nom
    e%prenom = prenom
    e%nb_concours = 0
    e%nb_voeux = 0
    e%affectation = 1

    creerEtudiant = e
  end function

  ! fonction pour recuperer le nom d'un etudiant
  function etudiantGetNom(e)
    type(t_etudiant),intent(in) :: e
    character(30) :: etudiantGetNom

    etudiantGetNom = e%nom
  end function

  ! fonction pour recuperer le prenom d'un etudiant
  function etudiantGetPrenom(e)
    type(t_etudiant),intent(in) :: e
    character(30) :: etudiantGetPrenom

    etudiantGetPrenom = e%prenom
  end function

  ! fonction pour recuperer le nombre de concours passé par un
  ! etudiant
  function etudiantGetNbConcours(e)
    type(t_etudiant),intent(in) :: e
    integer :: etudiantGetNbConcours

    etudiantGetNbConcours = e%nb_concours
  end function

  ! procedure pour ajouter une note à un etudiant
  subroutine etudiantAjouterNote(e,n)
    ! inout car on lit le nbre de note (lecture)
    ! et on l'incrémente (ecriture)
    type(t_etudiant),intent(inout) :: e
    type(t_note),intent(in)        :: n

    if(e%nb_concours < 5) then
      e%nb_concours = e%nb_concours + 1
      e%tab_note(e%nb_concours) = n
    else
      print *, "erreur : le nombre de note max est deja atteint"
    end if
  end subroutine

  ! procedure pour afficher toutes les notes d'un etudiant
  subroutine etudiantAfficherNoteEtudiant(e)
    type(t_etudiant),intent(in) :: e
    integer :: i

    if ( e%nb_concours > 0 ) then
      do i = 1,e%nb_concours,1
        print *, "-------------------"
        print *, e%tab_note(i)%note
        print *, e%tab_note(i)%c%nom
        print *, "-------------------"
      end do
    else
      print *,"erreur : cet etudiant n'a pas encore de note(s) associée(s)"
    end if
  end subroutine

  ! predicat qui renvoit vrai si l'étudiant à passé le concours
  function etudiantAPasse(e,c)
    type(t_etudiant), intent(in) :: e
    type(t_concours), intent(in) :: c
    logical :: res
    integer :: i
    logical :: etudiantAPasse

    res = .FALSE.
    i = 1

     !print *, "DEBUGAGE"
     !print *, e%nom, c%nom, e%nb_concours

    if (e%nb_concours > 0) then
      do while((res .eqv. .FALSE.) .AND. (i <= e%nb_concours))
        !print *, e%nom, " a passé le concours ",e%tab_note(i)%c%nom
        if (e%tab_note(i)%c%nom .eq. c%nom) then
          res = .TRUE.
        end if
        i = i + 1
      end do
    end if

    etudiantAPasse = res
  end function

  !fonction qui pour un tableau d'étudiant donné et un concours retourne
  !un tableau d'étudiant ayant passé ce CONCOURS
  subroutine etudiantAyantPasse(t,nb,c,res,nb_res)
    ! un tablau d'étudiants
    type(t_etudiant), dimension(:),intent(in)     :: t
    ! nbre d'étudiants dans le tableau
    integer                                       :: nb
    ! concours sur lequel on effectue la recherche
    type(t_concours)                                :: c
    ! un tablau d'étudiants
    type(t_etudiant), dimension(:),intent(inout)  :: res
    integer, intent(inout)                          :: nb_res
    integer                                       :: i
    type(t_etudiant) :: tmp

    nb_res = 0

    if (nb > 0) then
      do i = 1,nb,1

        tmp = t(i)
        if(etudiantAPasse(tmp,c)) then
          res(nb_res +1) = tmp
          nb_res = nb_res + 1
        end if
      end do
    else
      print *, "le tableau passé est vide"
    end if
  end subroutine

  function etudiantGetNoteConcours(e,c)
    type(t_etudiant), intent(in) :: e
    type(t_concours), intent(in) :: c
    real :: res
    real :: etudiantGetNoteConcours
    logical :: trouve
    integer :: i

    trouve = .FALSE.

    i = 1
    do while ((trouve .eqv. .FALSE.) .AND. (i <= e%nb_concours))
      if ( e%tab_note(i)%c%nom == c%nom ) then
        trouve = .TRUE.
        res = e%tab_note(i)%note
      end if
      i = i + 1
    end do
    etudiantGetNoteConcours = res
  end function

  subroutine etudiantTri(t,c,n)
    type(t_etudiant),dimension(:),intent(inout) :: t
    type(t_concours), intent(in) :: c
    integer, intent(in) :: n
    integer :: i
    integer :: j
    integer :: max
    type(t_etudiant):: swap

    do i = 1, n-1, 1
      max = i
      do j = i+1, n, 1
        if(etudiantGetNoteConcours(t(j),c) > etudiantGetNoteConcours(t(max),c)) then
          max = j
        end if
      end do
      if ( max /= i ) then
        swap = t(i)
        t(i) = t(max)
        t(max) = swap
      end if
    end do
  end subroutine

  ! procedure pour afficher les etudiants d'un tableau
  subroutine etudiantTableauAffichage(e,nb)
    type(t_etudiant), dimension(:),intent(in)     :: e
    integer,intent(in)                            :: nb
    integer                                       :: i
    integer :: j

    if (nb > 0) then

      do i=1,nb,1
        print *, e(i)%nom, e(i)%affectation
      end do
    else
      print *,"le tableau passé est vide"
    end if
  end subroutine

  ! procedure pour ajouter un etudiant à un tableau d'étudiant
  subroutine etudiantAjoutCollection(e,t,n)
    type(t_etudiant), intent(in) :: e
    type(t_etudiant),dimension(100),intent(inout) :: t
    integer, intent(inout) :: n

    if ( n < 100 ) then
      n = n + 1
      t(n) = e;
    else
      print *, "tableau rempli"
    end if
  end subroutine

  ! ajouter un tableau de voeux a un étudiant
  subroutine etudiantAjoutVoeux(e,t)
    type(t_etudiant), intent(inout) :: e
    type(t_formation),dimension(:), intent(in) :: t
    integer :: i

    if (size(t) <= 5) then
      do i = 1, size(t), 1
        e%tab_voeux(i) = t(i)
      end do
      e%nb_voeux = size(t)
    else
      print *, "il y a trop de voeux"
    end if
  end subroutine

  ! recuperer la formation correspondant au voeux n de l'eleve
  function etudiantNVoeux(e,n)
    type(t_etudiant), intent(in)  :: e ! un etudiant
    integer, intent(in)           :: n ! un index
    type(t_formation)             :: res
    type(t_formation)             :: etudiantNVoeux

    ! print *, "n vaut ", n
    ! print *, "etudiant nom : ", e%nom
    if (n <= e%nb_voeux) then
      res = e%tab_voeux(n)
      ! print *, res%nom
    else
      print *,"error : n > nb de voeux donné par l'utilisateur"
    end if
    etudiantNVoeux = res
  end function
end module Etudiant
