module affectation
  use Etudiant
  use Formation

  implicit none

  type t_affectation
    type(t_etudiant) :: etudiant
    type(t_formation):: formation
  end type t_affectation

contains

  ! permet d'ajouter une affectation à un tableau
  ! d'affectation
  subroutine affectationAjoutCollection(a,c,n)
    type(t_affectation), intent(inout) :: a
    type(t_affectation),dimension(100),intent(inout) :: c
    integer, intent(inout) :: n

    if ( n < 100 ) then
      n = n + 1
      c(n) = a;
    else
      print *, "tableau rempli"
    end if
  end subroutine

  ! permet d'afficher un tableau d'affectation
  subroutine affectationToString(c,n)
    type(t_affectation),dimension(100),intent(in) :: c
    integer, intent(in) :: n
    integer :: i

    do i = 1, n, 1
      print *, "nom ", c(i)%etudiant%nom," prenom ",c(i)%etudiant%prenom, " ecole ",c(i)%formation%nom
    end do
  end subroutine

  ! fonction qui renvoie un vecteur d'étudiant
  ! qui ne sont à l'instant t pas admissible pour
  ! une formation donnée
  ! CF : IN MAIN PROGRAM
  ! type(t_affectation),dimension(100),intent(in) :: tabEleveSurplus
  ! integer :: n_eleve_surplus
  subroutine affectationGetEleveEnSurplus(c1,n1,f,c2,n2)
    type(t_affectation),dimension(100),intent(in) :: c1
    type(t_etudiant),dimension(100),intent(inout) :: c2 ! tableau qu'on ecrase
    type(t_formation),intent(in) :: f
    integer,intent(in) :: n1
    integer,intent(out) :: n2 ! on a pas besoin de savoir sa valeur
    integer :: i

    n2 = 0
    do i = 1, n1, 1
      if(c1(i)%formation%nom == f%nom) then
        n2 = n2 + 1
        c2(n2) = c1(i)%etudiant
      end if
    end do
  end subroutine

  subroutine affectationIncrementAffectationEleve(t,e,n)
    type(t_affectation),dimension(100),intent(inout) :: t
    integer, intent(in) :: n
    type(t_etudiant) :: e
    integer :: i

    logical :: trouve
    i = 1;
    trouve = .FALSE.

    do while ((trouve .eqv. .FALSE.) .AND. (i <= n))
      if(t(i)%etudiant%nom == e%nom)then
        trouve = .TRUE.
        t(i)%etudiant%affectation = t(i)%etudiant%affectation + 1
      end if
      i = i + 1
    end do
  end subroutine


end module affectation
