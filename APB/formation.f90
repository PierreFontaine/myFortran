module formation

  implicit none

  type t_formation
    character(len=30) :: nom
    integer :: nb_place
    integer :: nb_eleve
  end type t_formation

contains

  function getFormationParNom(t,n,f)
    type(t_formation),dimension(:),intent(in) :: t
    integer,intent(in) :: n
    character(*) :: f
    type(t_formation) :: getFormationParNom

    logical :: trouve
    integer :: i
    type(t_formation) :: res

    i = 1
    trouve = .FALSE.

    do while ((trouve .eqv. .FALSE.) .AND. (i <= n))
      if(t(i)%nom == f) then
        res = t(i)
        trouve = .TRUE.
      end if
      i = i + 1
    end do
    getFormationParNom = res
  end function

  subroutine formationArrayToString(t,n)
    type(t_formation),dimension(:),intent(in) :: t
    integer,intent(in) :: n
    integer :: i

    do i = 1, n, 1
      print *, t(i)%nom ," ",t(i)%nb_place," ", t(i)%nb_eleve
    end do
  end subroutine

  ! recuperer le nom d'une formation
  function formationGetNom(f)
    character(30)                 :: formationGetNom
    type(t_formation), intent(in) :: f

    formationGetNom = f%nom
  end function

  ! fonction prenant en parametre un tableau de formation
  subroutine afficherFormation(f,n)
    implicit none
    type(t_formation),dimension(:),intent(in) :: f
    integer :: n
    integer :: i
    do i = 1, n, 1
      print *, f(i)%nom, "    ",f(i)%nb_place
    end do
  end subroutine afficherFormation

  ! fonction pour ajouter une formation à un tableau de formation
  subroutine ajouterFormation(nom,place,f,n)
    implicit none
    character(*), intent(in) :: nom
    integer,intent(in) :: place
    type(t_formation),dimension(:),intent(inout) :: f
    integer, intent(inout):: n

    type(t_formation) :: tmp

    tmp = t_formation(nom,place,0)

    if ( n+1 <= size(f) ) then
      f(n+1) = tmp
      n = n+1
    else
      print *,'plus de place pour stocker des formations'
    end if
  end subroutine ajouterFormation

  ! ajouter plusieurs formation avec un vecteur de formation
  subroutine ajouterFormations(t,n,v)
    type(t_formation),dimension(:),intent(inout) :: t
    type(integer),intent(inout) :: n
    type(t_formation),dimension(:),intent(in) :: v
    integer :: i

    if(size(v) <= 100) then
      do i = 1, size(v), 1
        t(i) = v(i)
      end do
      n = size(v)
    end if
  end subroutine

  subroutine formationIncrementPlaceEleve(t,f,n)
    type(t_formation),dimension(:), intent(inout) :: t
    type(t_formation), intent(in) :: f
    integer,intent(in) :: n
    logical :: trouve
    integer :: i

    i = 1

    trouve = .FALSE.

    do while ((trouve .eqv. .FALSE.) .AND. (i <= n))
      if (t(i)%nom == f%nom) then
        trouve = .TRUE.
        t(i)%nb_eleve = t(i)%nb_eleve + 1
      end if
      i = i + 1
    end do
  end subroutine

  subroutine formationDecrementPlaceEleve(t,f,n)
    type(t_formation),dimension(:), intent(inout) :: t
    type(t_formation), intent(in) :: f
    integer,intent(in) :: n
    logical :: trouve
    integer :: i

    i = 1

    trouve = .FALSE.

    do while ((trouve .eqv. .FALSE.) .AND. (i <= n))
      if (t(i)%nom == f%nom) then
        ! print *,"formation trouve"
        ! print *,"avant traitement : ",t(i)%nb_eleve
        ! print *,"index formation : ", i
        trouve = .TRUE.
        t(i)%nb_eleve = t(i)%nb_eleve - 1
        ! print *,"après traitement : ",t(i)%nb_eleve
      end if
      i = i + 1
    end do
  end subroutine

  ! fonction qui parcours un tableau de formation
  ! si elle rencontre une formation en surplus
  ! elle renvoi TRUE
  function formationSurplus(c,n)
    type(t_formation),dimension(100),intent(in) :: c
    integer,intent(in)                            :: n
    logical                                       :: formationSurplus
    logical :: res
    integer :: i

    res = .FALSE.
    i = 1

    do while ( (res .eqv. .FALSE.) .AND. (i <= n) )
      if(c(i)%nb_eleve > c(i)%nb_place) then
        ! print *, "formation ",c(i)%nom," en surcharge"
        res = .TRUE.
      end if
      i = i + 1
    end do
    formationSurplus = res;
  end function

  ! Cette fonction est appelé après avoir vérifié qu'il y ait
  ! une formation en surplus
  ! fonction qui parcours un tableau d'affectation
  ! si elle rencontre une formation en surplus
  ! elle renvoi la formation en question
  function formationGetSurplus(c,n)
    type(t_formation),dimension(100),intent(in) :: c
    integer,intent(in)                          :: n
    type(t_formation)                           :: formationGetSurplus
    type(t_formation) :: res
    logical :: trouve
    integer :: i

    trouve = .FALSE.
    i = 1

    do while ( (trouve .eqv. .FALSE.) .AND. (i <= n) )
      if(c(i)%nb_eleve > c(i)%nb_place) then
        print *, "formation ",c(i)%nom," en surcharge"
        print *, "nb_eleve : ",c(i)%nb_eleve
        print *, "nb_place : ",c(i)%nb_place
        res = c(i)
      end if
      i = i + 1
    end do
    formationGetSurplus = res;
  end function

end module formation
