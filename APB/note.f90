module note

  use concours

  implicit NONE

  type t_note
    real           :: note
    type(t_concours)  :: c
  end type t_note

end module note
